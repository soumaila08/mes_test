program chaleur

  implicit none

  integer, parameter:: rp=8
  integer:: N,M,i,ierr,j,ir
  integer:: t1,t2
  real(rp) ::xmax,ymax,x0,y0,alpha,temps
  real(rp)::t,t0,dt,dx,dy,cfl_x,cfl_y,tmax
  real(rp),dimension(:,:), allocatable::u
  real(rp),dimension(:,:),allocatable:: dx_u,dy_u

  !-----Attribution des variables-------!
  N=10000
  M=10000
  xmax=10._rp
  ymax=10._rp
  x0=0._rp
  y0=0._rp
  alpha=0.000127
  dt=0.5
  dx=(xmax-x0)/M
  dy=(ymax-y0)/N
  tmax=10._rp

  call system_clock(count=t1,count_rate=ir)
  !---Allocation des tableaux---------------------
  allocate(u(m+1,n+1),dx_u(1+m,1+n),dy_u(1+m,1+n),stat=ierr)
  if(ierr.ne.0) then
     print*,"pb allocation tableaux "
  end if

  !---Calcul des coefficient CFL------------
  cfl_x=alpha*dt/(dx*dx)
  cfl_y=alpha*dt/(dy*dy)



  !-------Initialisation de la solution u-----------
  do j=1,m+1
     do i=1,n+1
        u(i,j)=0_rp
     end do
  end do

  !-----Conditions aux limites-----------------------

  do i=1,m+1
     u(i,1)=35._rp
     u(i,n)=35._rp
  end do

  do j=1,n+1
     u(1,j)=10._rp
     u(m,j)=35._rp
  end do


  t0=0._rp
  do while(t<tmax)
     t=t+dt
     do j=2,m
        do i=2,n
           dx_u(i,j)=u(i+1,j)-2*u(i,j)+u(i-1,j)
           dy_u(i,j)=u(i,j+1)-2*u(i,j)+u(i,j-1)
        end do
     end do

     do j=2,n
        do i=2,m
           u(i,j)=u(i,j)+cfl_x*dx_u(i,j)+cfl_y*dy_u(i,j)
        end do
     end do
  end do
  call system_clock(count=t2,count_rate=ir)
  temps=real(t2-t1,kind=8)/real(ir,kind=8)

  print*, temps

  open (unit=34,file="Ufinal.txt")
  write(34,*) u
  close(34)
  
  deallocate(u,dx_u,dy_u)

end program chaleur




