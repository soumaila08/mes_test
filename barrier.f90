program barrier

  !$ USE OMP_LIB
  IMPLICIT NONE
  integer :: num_thread

  !$OMP PARALLEL PRIVATE(num_thread)
  num_thread=OMP_GET_THREAD_NUM();
  !$OMP SINGLE
  write(*,*) "single hello depuis le thread:",num_thread
  !$OMP END SINGLE NOWAIT

  write(*,*) "regin parallele hello depuis le thread", num_thread
  

  !$OMP MASTER
  write(*,*) "master hello depuis le thread", num_thread
  !$OMP END MASTER
  !$OMP END PARALLEL

end program barrier


