program fonction_openmp
  !$ USE OMP_LIB

  IMPLICIT NONE

  integer:: nbr_procs
  integer:: nbr_threads
  integer:: num_threads
  real   :: t_0,t_1,tfinal
  logical:: sequences

  t_0=omp_get_wtime()
  nbr_procs=omp_get_num_procs()
  sequences=omp_in_parallel()

  write(*,*) "sequences=",omp_in_parallel()

  write(*,*) "nombre de processus=",nbr_procs
  !$OMP PARALLEL
  nbr_threads=omp_get_num_threads()
  !$OMP END PARALLEL

  write(*,*) "nbre de threads utilisé=", nbr_threads

  t_1=omp_get_wtime()

  tfinal=t_1-t_0

  write(*,*) "temps final =",tfinal




end program fonction_openmp

  
  
