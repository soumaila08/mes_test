program pi_calcul
 !$ USE OMP_LIB
  implicit none
  integer::i,N,j
  integer,parameter::rp=8
  real(rp) ::a,b,h,pi_approch,temps
  real(rp),dimension(:),allocatable ::x
  integer :: t1,t2,ir,nbr_procs


  a=0._rp
  b=1._rp
  N=100000
  h=(b-a)/N

  call system_clock(count=t1,count_rate=ir)
  allocate(x(N))
  
  !$ nbr_procs=omp_get_num_procs()
  !$ write(*,*)" le nombre de procs dispo=",nbr_procs
  !$OMP PARALLEL PRIVATE(i,x) FIRSTPRIVATE(N)
  !$ call omp_set_num_threads(4)
  !$OMP DO
  do i=1,N
     x(i)=a+i*h
  end do
  !$OMP END DO 
  pi_approch=0._rp
  
  !$OMP DO REDUCTION(+:pi_approch)
  do i=1,N
     pi_approch=pi_approch+h*f(x(i))
  end do
  !$OMP END DO
  !$OMP END PARALLEL
  write(*,*)" pi_approche=",pi_approch
  call system_clock(count=t2,count_rate=ir)
  temps=real(t2-t1,kind=8)/real(ir,kind=8)

  print*,"tempe en parallele=",temps

  deallocate(x)

contains
  function f(x)
    implicit none
    integer,parameter::rp=8
    real(rp) ::x,f
    f=4._rp/(1+x**2)
  end function f
end program pi_calcul


